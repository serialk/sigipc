#define _GNU_SOURCE 1

#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

static struct
{
    char* data;
    size_t pos;
    size_t len;
    unsigned current_bit;
    unsigned signal_wait;
} g_sigipc_r;

static unsigned g_sigipc_s; // set if ack received

static void sigipc_handle_(int signum)
{
    g_sigipc_r.current_bit = ((signum == SIGUSR1) ? 0 : 1);
    g_sigipc_r.signal_wait = 0;
}

static void sigipc_ack_(int signum)
{
    (void) signum;
    g_sigipc_s = 0;
}

/// Receive a null-terminated string from the process running with PID \a pid
/// \return the null-terminated string sent by the sender process.
/// The caller should free(3) the returned string.
static char *sigipc_recv(pid_t pid)
{
    g_sigipc_r.data = (char *) malloc(8 * sizeof (char));
    g_sigipc_r.len = 8;
    g_sigipc_r.pos = 0;

    struct sigaction act, oldact;
    act.sa_flags = 0;
    act.sa_handler = sigipc_handle_;
    sigemptyset(&act.sa_mask);
    sigaction(SIGUSR1, &act, &oldact);
    sigaction(SIGUSR2, &act, NULL);

    sigset_t mask, oldmask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGUSR2);
    while (1)
    {
        unsigned byte = 0;
        for (unsigned i = 0; i < 8; i++)
        {
            g_sigipc_r.signal_wait = 1;
            sigprocmask(SIG_BLOCK, &mask, &oldmask);
            while (g_sigipc_r.signal_wait)
                sigsuspend(&oldmask);
            sigprocmask(SIG_UNBLOCK, &mask, NULL);

            // current_bit is now set and ready to be used
            byte = byte | (g_sigipc_r.current_bit << i);

            // send ack to the server
            usleep(50);
            kill(pid, SIGUSR1);
        }

        if (g_sigipc_r.pos >= g_sigipc_r.len)
        {
            g_sigipc_r.len *= 2;
            g_sigipc_r.data = (char *) realloc(g_sigipc_r.data,
                                               g_sigipc_r.len * sizeof (char));
        }

        g_sigipc_r.data[g_sigipc_r.pos++] = byte;

        if (!byte) // \0 -> EOF
            break;
    }

    sigaction(SIGUSR1, &oldact, NULL);
    sigaction(SIGUSR2, &oldact, NULL);
    return g_sigipc_r.data;
}

/// Send the null-terminated string \a data to the process running
/// with PID \a pid.
/// This should be called after the receiving process call sigipc_recv(), else
/// the receiving process will not have the signal handlers installed and will
/// be terminated by the incoming signals.
static void sigipc_send(pid_t pid, const char *data)
{
    struct sigaction act, oldact;
    act.sa_flags = 0;
    act.sa_handler = sigipc_ack_;
    sigemptyset(&act.sa_mask);
    sigaction(SIGUSR1, &act, &oldact);

    sigset_t mask, oldmask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);

    for (const char *c = data; *c || (c > data && *(c - 1)); c++)
        for (unsigned i = 0; i < 8; i++)
        {
            unsigned bit = (*c >> i) & 1;
            int signum = (bit ? SIGUSR2 : SIGUSR1);
            usleep(50);
            kill(pid, signum);

            // wait for the ack
            g_sigipc_s = 1;
            sigprocmask(SIG_BLOCK, &mask, &oldmask);
            while (g_sigipc_s)
                sigsuspend(&oldmask);
            sigprocmask(SIG_UNBLOCK, &mask, NULL);
        }

    sigaction(SIGUSR1, &oldact, NULL);
}
