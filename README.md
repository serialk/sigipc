# sigipc : Inter Process Communication with Signals

Everyone heard that joke.

> Well, technically, you *can* send data with signals, if you send binary data
> using a series of ``SIGUSR1`` and ``SIGUSR2`` as 0 and 1.

This is a proof of concept.

# Usage

    :::c
    #include <sigipc.h>

    char *sigipc_recv(pid_t pid)

Receive a null-terminated string from the process running with PID ``pid``.

Returns the null-terminated string sent by the sender process.
The caller should ``free(3)`` the returned string.

    :::c
    void sigipc_send(pid_t pid, const char *data)

Send the null-terminated string ``data`` to the process running with PID
``pid``.

This should be called after the receiving process call ``sigipc_recv()``, else
the receiving process will not have the signal handlers installed and will
be terminated by the incoming signals.

# How does it work?

It sends a series of signals to the receiving process. ``SIGUSR1`` means 0,
``SIGUSR2`` means 1.

You can't just send a series of signals, because the kernel might change their
order or worse, *merge* identical signals into one. Every time we receive a
bit, we need to send an ack to the server (``SIGUSR1``).

# Example

An usage example can be found in test.c. Try it with:

    gcc -std=c99 test.c -o test
    ./test

# Should I use this for…

No.
