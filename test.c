#include "sigipc.h"
#include <unistd.h>

int main(void)
{
    static char *string = "login";

    int pid = getpid();
    int cid = fork();
    if (cid)
    {
        sleep(1);
        sigipc_send(cid, string);
    }
    else
    {
        char *s = sigipc_recv(pid);
        printf("%s\n", s);
        free(s);
    }
    return 0;
}
